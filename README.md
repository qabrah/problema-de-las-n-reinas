# Problema de las N Reinas

El problema de las n reinas, consiste en colocar n cantidad de reinas en un tablero de ajedrez, sin que ninguna de ellas puedan comerse. 

solucion propuesta en python

# Documentacion 
La solucion mostrada para el problema utilizo los siguientes medios como documentacion:
    1.-https://github.com/danielTeniente/guia_de_competencia/blob/master/Vegas/N-queens.ipynb
    2.-http://webdiis.unizar.es/asignaturas/APD/wp/wp-content/uploads/2013/09/           161017-algoritmos-Probabilistas.pdf (paginas 100 a 119)
    3.-https://www.youtube.com/watch?v=nsbHNy7yWic

Se tomo la eleccion del algoritmo de las vegas para este problema ya que los algoritmos clasicos no iban a generar un solucion debido al tamaño de comparaciones necesarias para una solucion.
